# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* generic_tag_automation
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 11.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-12 17:25+0000\n"
"PO-Revision-Date: 2019-09-12 17:25+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: generic_tag_automation
#: model:ir.model.fields,field_description:generic_tag_automation.field_base_automation_act_add_tag_ids
msgid "Add Tags"
msgstr ""

#. module: generic_tag_automation
#: model:ir.model,name:generic_tag_automation.model_base_automation
msgid "Automated Action"
msgstr ""

#. module: generic_tag_automation
#: model:ir.model.fields,field_description:generic_tag_automation.field_base_automation_act_remove_tag_ids
msgid "Remove Tags"
msgstr ""

#. module: generic_tag_automation
#: model:ir.model.fields,help:generic_tag_automation.field_base_automation_act_add_tag_ids
msgid "Specify tags to be added to object this rule is applied to"
msgstr ""

#. module: generic_tag_automation
#: model:ir.model.fields,help:generic_tag_automation.field_base_automation_act_remove_tag_ids
msgid "Specify tags to be removed from object this rule is applied to"
msgstr ""

#. module: generic_tag_automation
#: model:ir.model,name:generic_tag_automation.model_ir_actions_server
msgid "ir.actions.server"
msgstr ""

